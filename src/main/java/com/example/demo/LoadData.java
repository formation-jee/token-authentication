package com.example.demo;

import com.example.demo.repository.CandidatRepository;
import com.example.demo.model.Candidat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Configuration
class LoadData {
    private static final Logger log = LoggerFactory.getLogger(LoadData.class);

    @Bean
    CommandLineRunner initDatabase(CandidatRepository candidatRepository) throws ParseException {
        if(candidatRepository.count() == 0){
            log.info("Création d'un candidat");
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date birthCandidat1 = formatter.parse("30/03/1993");

            Candidat candidat = new Candidat("candidat1", "nom candidat 1",
                    birthCandidat1, "Place de Jaude", "Clermont-Ferrand", "63000");


            return args -> {
                log.info("Preloading " + candidatRepository.save(candidat));
                log.info("Preloading " + candidatRepository.save(candidat));
            };
        } else {

            return args -> {
                log.info("Already initialized");
            };
        }

    }
}


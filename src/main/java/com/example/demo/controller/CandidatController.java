package com.example.demo.controller;
import com.example.demo.repository.CandidatRepository;
import com.example.demo.model.Candidat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/candidats")
public class CandidatController {
    @Autowired
    private CandidatRepository candidatRepository;

    @GetMapping("/")
    List<Candidat> all(){
        return candidatRepository.findAll();
    }

    @GetMapping(value = "/{candidat}", produces = {"application/json"})
    Candidat getOne(@PathVariable(name="candidat", required = false) Candidat candidat){
        if (candidat == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat introuvable"
            );
        } else {
            return candidat;
        }
    }

    @PostMapping(value = "/", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity<Candidat> saveCandidat(@Valid @RequestBody Candidat candidat, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bindingResult.toString());
        } else {
            this.candidatRepository.save(candidat);
            return new ResponseEntity<Candidat>(candidat, HttpStatus.OK);
        }
    }

    @PutMapping(value = "/{candidat}", produces = {"application/json"})
    public ResponseEntity<Candidat> update(@PathVariable(name="candidat", required = false) Candidat candidat,
                                           @Valid @RequestBody Candidat candidatUpdate) {
            candidatUpdate.setId(candidat.getId());
            this.candidatRepository.save(candidatUpdate);
            return new ResponseEntity<Candidat>(candidat, HttpStatus.OK);
    }


    @DeleteMapping(value = "/{candidat}", produces = {"application/json"})
    void deleteOne(@PathVariable(name="candidat", required = false) Candidat candidat){
        if (candidat == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat introuvable"
            );
        } else {
            this.candidatRepository.delete(candidat);
        }
    }

}

   /* private final CandidatRepository candidatRepository;
    public CandidatController(CandidatRepository candidatRepository) {
        this.candidatRepository = candidatRepository;
    }


    @GetMapping("/")
    List<Candidat> all(){
        return candidatRepository.findAll();
    }

    @GetMapping(value = "/{candidat}", produces = {"application/json"})
    Candidat getOne(@PathVariable(name="candidat", required = false) Candidat candidat){
        if (candidat == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat introuvable"
            );
        } else {
            return candidat;
        }
    }

    @PostMapping(value = "/", produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity<Candidat> saveCandidat(@Valid @RequestBody Candidat candidat, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
           throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bindingResult.toString());
        } else {
           this.candidatRepository.save(candidat);
            return new ResponseEntity<Candidat>(candidat, HttpStatus.OK);
        }
    }

    @PutMapping(value = "/{candidat}", produces = {"application/json"})
    public ResponseEntity<Candidat> update(@PathVariable(name="candidat", required = false) Candidat candidat,
                                           @Valid @RequestBody Candidat candidatUpdate, BindingResult bindingResult) {
        if (candidat == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat introuvable"
            );
        }

        if (bindingResult.hasErrors()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bindingResult.toString());
        } else {
            candidatUpdate.setId(candidat.getId());
            this.candidatRepository.save(candidatUpdate);

            return new ResponseEntity<Candidat>(candidat, HttpStatus.OK);
        }
    }

    @DeleteMapping(value = "/{candidat}", produces = {"application/json"})
    void deleteOne(@PathVariable(name="candidat", required = false) Candidat candidat){
        if (candidat == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Candidat introuvable"
            );
        } else {
            this.candidatRepository.delete(candidat);

        }
    }
*/




package com.example.demo.controller;

import com.example.demo.repository.UserRepository;
import com.example.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@CrossOrigin
public class SecurityController {
        @Autowired
        private UserRepository userRepository;

        @Autowired
        private BCryptPasswordEncoder passwordEncoder;

        @PostMapping("/register")
        public void signUp(@RequestBody User user) {
            if( this.userRepository.findByUsername(user.getUsername()) != null  ) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Utilisateur déjà connu");
            }
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(user);
        }

    @GetMapping("/authenticated")
    public Authentication authenticated() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
    }

package com.example.demo.repository;

import com.example.demo.model.Candidat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CandidatRepository extends CrudRepository<Candidat, Long> {
    @Override
    List<Candidat> findAll();
}

